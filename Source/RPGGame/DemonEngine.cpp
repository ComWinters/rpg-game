// Fill out your copyright notice in the Description page of Project Settings.

#include "DemonEngine.h"
#include "RPGGame.h"
#include "GameCharacter.h"

DemonEngine::DemonEngine(TArray<UGameCharacter*> playerParty, TArray<UGameCharacter*> enemyParty)
{
	this->playerParty = playerParty;
	this->enemyParty = enemyParty;

	/*Orders players*/
	for (int i = 0; i < playerParty.Num(); i++)
	{
		this->combatantOrder.Add(playerParty[i]);
	}

	/*Orders enemies*/
	for (int i = 0; i < enemyParty.Num(); i++)
	{
		this->combatantOrder.Add(enemyParty[i]);
	}

	for (int i = 0; i < this->combatantOrder.Num(); i++)
	{
		this->combatantOrder[i]->combatInstance = this;
	}

	/*Sets tick target to the first character in the combat order*/
	this->tickTargetIndex = 0;
	this->SetPhase(CombatPhase::CPHASE_Choice);
}

DemonEngine::~DemonEngine()
{
	for (int i = 0; i < this->enemyParty.Num(); i++)
	{
		this->enemyParty[i] = nullptr;
	}

	for (int i = 0; i < this->combatantOrder.Num(); i++)
	{
		this->combatantOrder[i]->combatInstance = nullptr;
	}
}

bool DemonEngine::Tick(float DeltaSeconds)
{
	switch (phase)
	{
	case CombatPhase::CPHASE_Choice:
	{
		if (!this->waitingForCharacter)
		{
			this->currentTickTarget->BeginMakeDecision();
			this->waitingForCharacter = true;
		}
		/* If waiting for character is true, call beginmakedecision and set waiting for character to true*/
		bool decisionMade = this->currentTickTarget->MakeDecision(DeltaSeconds);

		if (decisionMade)
		{
			SelectNextCharacter();

			if (this->tickTargetIndex == -1)
			{
				this->SetPhase(CombatPhase::CPHASE_Action);
			}
		}
	}
	break;

	case CombatPhase::CPHASE_Action:
	{
		if (!this->waitingForCharacter)
		{
			this->currentTickTarget->BeginExecuteAction();
			this->waitingForCharacter = true;
		}

		bool actionFinished = this->currentTickTarget->ExecuteAction(DeltaSeconds);

		if (actionFinished)
		{
			SelectNextCharacter();

			if (this->tickTargetIndex == -1)
			{
				this->SetPhase(CombatPhase::CPHASE_Choice);
			}
		}
	}
	break;

	case CombatPhase::CPHASE_Defeat:
	case CombatPhase::CPHASE_Victory:
		return true;
		break;
	}

	/*Player loses*/
	int deadCount = 0;
	for (int i = 0; i < this->playerParty.Num(); i++)
	{
		if (this->playerParty[i]->HP <= 0) deadCount++;
	}

	if (deadCount == this->playerParty.Num())
	{
		this->SetPhase(CombatPhase::CPHASE_Defeat);
		return false;
	}

	/*Player wins*/
	deadCount = 0;
	for (int i = 0; i < this->enemyParty.Num(); i++)
	{
		if (this->enemyParty[i]->HP <= 0) deadCount++;
	}

	if (deadCount == this->enemyParty.Num())
	{
		this->SetPhase(CombatPhase::CPHASE_Victory);
		return false;
	}

	return false;
}

void DemonEngine::SetPhase(CombatPhase phase)
{
	this->phase = phase;

	switch (phase)
	{
	case CombatPhase::CPHASE_Action:
	case CombatPhase::CPHASE_Choice:
		this->tickTargetIndex = 0;
		this->SelectNextCharacter();
		break;
	case CombatPhase::CPHASE_Victory:
		break;
	case CombatPhase::CPHASE_Defeat:
		break;
	}
}

void DemonEngine::SelectNextCharacter()
{
	this->waitingForCharacter = false;
	for (int i = this->tickTargetIndex; i < this->combatantOrder.Num(); i++)
	{
		UGameCharacter* character = this->combatantOrder[i];

		if (character->HP > 0)
		{
			this->tickTargetIndex = i + 1;
			this->currentTickTarget = character;
			return;
		}
	}
	this->tickTargetIndex = -1;
	this->currentTickTarget = nullptr;
}
