// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGPlayerController.h"
#include "RPGGame.h"
#include "ControllableCharacter.h"

void ARPGPlayerController::MoveVertical(float Value)
{
	/*Points to pawn that controller is in control of*/
	IControllableCharacter* pawn = Cast<IControllableCharacter>(
		GetPawn());
	/*Checks if pointer is null. if not, call MoveVertical*/
	if (pawn != NULL)
	{
		pawn->MoveVertical(Value);
	}
}

void ARPGPlayerController::MoveHorizontal(float Value)
{
	/*Points to pawn that controller is in control of*/
	IControllableCharacter* pawn = Cast<IControllableCharacter>(
		GetPawn());
	/*Checks if pointer is null. if not, call MoveHorizontal*/
	if (pawn != NULL)
	{
		pawn->MoveHorizontal(Value);
	}
}
void ARPGPlayerController::SetupInputComponent()
{
	/*If no input, make new input class to pass type*/
	if (InputComponent == NULL)
	{
		InputComponent = NewObject<UInputComponent>(this);
		InputComponent->RegisterComponent();
	}

	/*Binds movement axes*/
	InputComponent->BindAxis("MoveVertical", this, &ARPGPlayerController::MoveVertical);
	InputComponent->BindAxis("MoveHorizontal", this, &ARPGPlayerController::MoveHorizontal);

	/*Shows or hides the mouse cursor*/
	this->bShowMouseCursor = true;

}