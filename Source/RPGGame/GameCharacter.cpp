// Fill out your copyright notice in the Description page of Project Settings.

#include "GameCharacter.h"
#include "DemonEngine.h"
#include "TestCombatAction.h"
#include "RPGGame.h"

UGameCharacter::UGameCharacter(const class FObjectInitializer&
	objectInitializer)
	:Super(objectInitializer)
{
}

/*Pointer to FCharacterInfo struct*/
UGameCharacter* UGameCharacter::CreateGameCharacter(FCharacterInfo* characterInfo, UObject* outer)
{
	/*Return from data and outer object*/
	UGameCharacter* character = NewObject<UGameCharacter>(outer);

	/*find character classes*/
	UDataTable* characterClasses = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), NULL, TEXT("DataTable'/Game/Tables/CharacterStatInfo.CharacterStatInfo'")));

	/*Try and find character class data table*/
	if (characterClasses == NULL)
	{
		UE_LOG(LogTemp, Error, TEXT("Character classes datatable not found"));
	}
	/*If succesful find the proper row and initialize field*/
	else
	{
		character->CharacterName = characterInfo->Character_Name;
		FCharacterClassInfo* row = characterClasses->FindRow
			<FCharacterClassInfo>(*(characterInfo->Class_ID), TEXT("LookupCharacterClass"));
		character->ClassInfo = row;

		character->MHP = character->ClassInfo->StartMHP;
		character->MMP = character->ClassInfo->StartMMP;
		character->HP = character->MHP;
		character->MP = character->MMP;

		character->ATK = character->ClassInfo->StartATK;
		character->DEF = character->ClassInfo->StartDEF;
		character->ST = character->ClassInfo->StartST;
		character->MA = character->ClassInfo->StartMA;
		character->VI = character->ClassInfo->StartVI;
		character->AG = character->ClassInfo->StartAG;
		character->LUCK = character->ClassInfo->StartLuck;
	}

	return character;
}

UGameCharacter* UGameCharacter::CreateGameCharacter(FEnemyInfo* enemyInfo, UObject* outer)
{
	UGameCharacter* character = NewObject<UGameCharacter>(outer);

	character->CharacterName = enemyInfo->EnemyName;
	character->ClassInfo = nullptr;

	character->MHP = enemyInfo->MHP;
	character->MMP = 0;
	character->HP = enemyInfo->MHP;
	character->MP = 0;

	character->ATK = enemyInfo->ATK;
	character->DEF = enemyInfo->DEF;
	character->LUCK = enemyInfo->Luck;

	return character;
}
/* These following lines of code is for debugging purposes, it will log a message and delay for one second. */
void UGameCharacter::BeginMakeDecision()
{
	UE_LOG(LogTemp, Log, TEXT("Character %s making decision"), *this->CharacterName);
	this->combatAction = new TestCombatAction();}

bool UGameCharacter::MakeDecision(float DeltaSeconds)
{
	return true;
}

void UGameCharacter::BeginExecuteAction()
{
	UE_LOG(LogTemp, Log, TEXT("Character %s executing action"), *this->CharacterName);
	this->combatAction->BeginExecuteAction(this);
}

bool UGameCharacter::ExecuteAction(float DeltaSeconds)
{
	bool finishedAction = this->combatAction->ExecuteAction(DeltaSeconds);
	if (finishedAction)
	{
		delete(this->combatAction);
		return true;
	}
	return false;
}

/* end of debugging */

void UGameCharacter::BeginDestroy()
{
	Super::BeginDestroy();
}
