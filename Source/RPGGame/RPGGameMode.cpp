// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGGameMode.h"
#include "RPGPlayerController.h"
#include "RPGCharacter.h"
#include "StrangeGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "RPGGame.h"

ARPGGameMode::ARPGGameMode(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PlayerControllerClass = ARPGPlayerController::StaticClass();
	DefaultPawnClass = ARPGCharacter::StaticClass();
}

void ARPGGameMode::BeginPlay()
{
	Cast<UStrangeGameInstance>(GetGameInstance())->Init();
}

/*See id there is an instance of combat, if there is, call instance tick method and check for victory/defeat*/
void ARPGGameMode::Tick(float DeltaTime)
{
	if (this->currentCombatInstance != nullptr)
	{
		bool combatOver = this->currentCombatInstance->Tick(DeltaTime);

		if (combatOver)
		{
			if (this->currentCombatInstance->phase == CombatPhase::CPHASE_Defeat)
			{
				UE_LOG(LogTemp, Log, TEXT("Your journey ends here..."));
			}
			else if (this->currentCombatInstance->phase == CombatPhase::CPHASE_Victory)
			{
				UE_LOG(LogTemp, Log, TEXT("Victory Achived!"));
			}

			UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetActorTickEnabled(true);

			delete(this->currentCombatInstance);
			this->currentCombatInstance = nullptr;
			this->enemyParty.Empty();
		}
	}
}

void ARPGGameMode::TestCombat()
{
	//Find enemy asset
	UDataTable* enemyTable = Cast <UDataTable>(StaticLoadObject(UDataTable::StaticClass(), NULL, TEXT("DataTable'/Game/Tables/EnemyInfoTable.EnemyInfoTable'")));

	if (enemyTable == NULL)
	{
		UE_LOG(LogTemp, Error, TEXT("Enemy data table was not found"));
		return;
	}

	FEnemyInfo* row = enemyTable->FindRow<FEnemyInfo>(TEXT("S1"), TEXT("LookupEnemyInfo"));

	if (row == NULL)
	{
		UE_LOG(LogTemp, Error, TEXT("Enemy ID 'S1' was not found"));
		return;
	}

	//Disable player actor
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetActorTickEnabled(false);

	//Populate enemy party
	UGameCharacter* enemy = UGameCharacter::CreateGameCharacter(row, this);
	this->enemyParty.Add(enemy);

	UStrangeGameInstance* gameInstance = Cast<UStrangeGameInstance>(GetGameInstance());

	this->currentCombatInstance = new DemonEngine(gameInstance->PartyMembers, this->enemyParty);

	UE_LOG(LogTemp, Log, TEXT("Battle Begin"));
}