// Fill out your copyright notice in the Description page of Project Settings.

#include "TestCombatAction.h"
#include "RPGGame.h"


void TestCombatAction::BeginExecuteAction(UGameCharacter* character)
{
	UE_LOG(LogTemp, Log, TEXT("%s does nothing"), *character->CharacterName);
	this->delayTimer = 1.0f;
}

bool TestCombatAction::ExecuteAction(float DeltaSeconds)
{
	this->delayTimer -= DeltaSeconds;
	return this->delayTimer <= 0.0f;
}