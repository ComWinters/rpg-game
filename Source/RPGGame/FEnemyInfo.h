// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "FEnemyInfo.generated.h"

USTRUCT(BlueprintType)
struct FEnemyInfo : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		FString EnemyName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		int32 MHP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		int32 MMP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		int32 ATK;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		int32 DEF;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		int32 ST;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		int32 MA;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		int32 VI;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		int32 AG;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		int32 Luck;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "FEnemyInfo")
		TArray<FString> Abilities;

};
