// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "RPGCameraComp.generated.h"

/*Allows custom comp to show up in blueprint*/
UCLASS(meta = (BlueprintSpawnableComponent))
class RPGGAME_API URPGCameraComp : public UCameraComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraProperties)
		/*Angle of camera*/
		float CameraPitch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CameraProperties)
		/*Distance from pawn*/
		float CameraDistance;

	/*Calculates posistion, rotation etc.*/
	virtual void GetCameraView(float DeltaTime, FMinimalViewInfo& DesiredView) override;
};