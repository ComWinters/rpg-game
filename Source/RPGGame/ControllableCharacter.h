// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ControllableCharacter.generated.h"

UINTERFACE()
class RPGGAME_API UControllableCharacter : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class RPGGAME_API IControllableCharacter
{
	GENERATED_IINTERFACE_BODY()

	virtual void MoveVertical(float Value);
	virtual void MoveHorizontal(float Value);
};