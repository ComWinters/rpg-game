// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameCharacter.h"
#include "Engine/GameInstance.h"
#include "StrangeGameInstance.generated.h"

UCLASS()
class RPGGAME_API UStrangeGameInstance : public UGameInstance
{
	GENERATED_BODY()

	UStrangeGameInstance(const class FObjectInitializer& objectInitializer);

public:
	TArray<UGameCharacter*> PartyMembers;

protected:
	bool isInitialized;

public:
	void Init();
};
