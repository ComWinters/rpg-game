// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RPGGame.h"
#include "GameCharacter.h"

enum class CombatPhase : uint8
{
	CPHASE_Choice, //Choice phase of combat
	CPHASE_Action, //Action phase of combat
	CPHASE_Victory, //Victory phase of combat if all enemies are wiped out/
	CPHASE_Defeat, //Defeat phase of combat if all party members are wiped out.
};

class RPGGAME_API DemonEngine
{
	/*Instaces three TArrays for combat*/
public:
	TArray<UGameCharacter*> combatantOrder;
	TArray<UGameCharacter*> playerParty;
	TArray<UGameCharacter*> enemyParty;

	CombatPhase phase;

protected:
	/*Points to character doing actions in combat, once done tick will return true and move to next character*/
	UGameCharacter * currentTickTarget;
	int tickTargetIndex;

	/*Calls tick every frame of combat, set to true once combat is finished */
public:
	DemonEngine(TArray<UGameCharacter*> playerParty, TArray<UGameCharacter*> enemyParty);
	~DemonEngine();

	bool waitingForCharacter;
	bool Tick(float DeltaSeconds);

protected:
	void SetPhase(CombatPhase phase);
	void SelectNextCharacter();
};