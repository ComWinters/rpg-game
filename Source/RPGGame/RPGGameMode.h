// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "DemonEngine.h"
#include "RPGGameMode.generated.h"

UCLASS()
class RPGGAME_API ARPGGameMode : public AGameMode
{
	GENERATED_BODY()

	ARPGGameMode(const class FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

public: 
	DemonEngine * currentCombatInstance;
	TArray<UGameCharacter*> enemyParty;

	UFUNCTION(exec)
	void TestCombat();
};