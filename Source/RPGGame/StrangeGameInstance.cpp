// Fill out your copyright notice in the Description page of Project Settings.

#include "StrangeGameInstance.h"

UStrangeGameInstance::UStrangeGameInstance(const class FObjectInitializer& objectInitializer)
	:Super(objectInitializer)
{

}


void UStrangeGameInstance::Init()
{
	if (this->isInitialized) return;
	
	this->isInitialized = true;

	/*Locate assets*/
	UDataTable* characters = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), NULL, TEXT("DataTable'/Game/Tables/Characters.Characters'")));

	if (characters == NULL)
	{
		UE_LOG(LogTemp, Error, TEXT("Data table not found."));
		return;
	}

	/*Located character*/
	FCharacterInfo* row = characters->FindRow<FCharacterInfo>(TEXT("S1"), TEXT("LookupCharacterClass"));

	if (row == NULL)
	{
		UE_LOG(LogTemp, Error, TEXT("ID S1 not found"));
		return;
	}

	/*Add character to party*/
	this->PartyMembers.Add(UGameCharacter::CreateGameCharacter(row, this));
}