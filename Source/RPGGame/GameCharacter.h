// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FCharacterInfo.h"
#include "FCharacterClassInfo.h"
#include "FEnemyInfo.h"
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/DataTable.h"
#include "GameCharacter.generated.h"

class DemonEngine;

UCLASS(BlueprintType)


class RPGGAME_API UGameCharacter : public UObject
{
	GENERATED_BODY()
	
public:

	UGameCharacter(const class FObjectInitializer&);

	FCharacterClassInfo * ClassInfo;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		FString CharacterName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 MHP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 MMP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 HP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 MP;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 ATK;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 DEF;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 ST;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 MA;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 VI;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 AG;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CharacterInfo")
		int32 LUCK;

public:
	static UGameCharacter* CreateGameCharacter(FCharacterInfo* characterInfo, UObject* outer);
	void BeginMakeDecision();
	bool MakeDecision(float DeltaSeconds);

	void BeginExecuteAction();
	bool ExecuteAction(float DeltaSeconds);

	static UGameCharacter* CreateGameCharacter(FEnemyInfo* enemyInfo, UObject* outer);

	DemonEngine* combatInstance;

	class ICombatAction *combatAction;

protected:
	
public:
	void BeginDestroy() override;
};
